<h1>Introduction</h1>

<p>
   <a href="https://microservices.io/">Microservice</a> architecture is a great 
   way to design scalable and robust softwares.Basically microservice is network 
   of services interacting and co-operating with each other using REST over HTTP 
   to achieve a common goal keeping development independent. Each services are 
   running on different host and possibly on different port. This makes the 
   architecture more complex because of locating the service is difficult. In the 
   network of services there exist one service which actually interact with user
   and providing the UI interface to interact with the system. This single service 
   is mostly communicating with all other services available in the network. 
   Though in microservice architecture is not restricting one service to interact 
   with other one, keeping this restriction, that only UI-service will interact 
   with all other service and no other service is required to communicate with 
   other, gives the architecture a simple shape.<br>
   Also the microservice architecture allow service to start or shutdown at any
   specific time. That means one service may be available at one point of time 
   but may be down at other point of time. Again the service many be shifted 
   to different host and different port.
  </p>
  
  <p>
      If the service is allowed to shift on different host, it is hard for other 
      service to locate it. And in this case the other services start failing.
      This nature of microservice architecture imposes the requirement of having 
      an registry from where the service can be discovered dynamically and 
      can be registered dynamically. The <a href="https://github.com/Netflix/eureka">
   Eureka</a> is fulfilling the exactly same requirement.
   So each service, as they get started, must register itself with eureka server.
   When a service want any other service then it ask eureka to provide the location.
   Eureka also keep track of liveliness of services using the heartbeat signal.
   So one service is down then eureka can figure it out as no hertbeat signal is 
   received by eureka. If a service want that service, eureka can inform that it
   is down. In this case the demanding or requesting sevice can handle it 
   gracefully.
  </p>
  
<p>
   <h3>Problem with Eureka</h3>
   Eureka is greate for dynamic service discovery but it has its own problem. Just consider the following case:<br>
   
   If imposing the above discussed architecture, the UI service has to communicate
   with many services. And hence many different URLs are reuired to make this 
   communication possible. If service get a wrong URL then it will either start 
   giving wrong result or throw 404 error. <br>
   To simplify the development, a uniform way is required to discover the service.
   Here the Eureka will not come for rescue. A proxy server is required to solve 
   this issue. A proxy server makes URL uniform, so that requesting service has 
   to deal with <b>"SINGLE TYPE OF URL"</b>.
   <a href="https://github.com/Netflix/zuul">Netflix Zuul</a> is used for this 
   purpose only.
</p>

<h1>How to run the application</h1>

<p>
   The application is <a href="https://maven.apache.org/">Maven</a> based spring 
   boot project.
   Download the <a href="https://maven.apache.org/download.cgi">latest release</a> maven and install.
   Set up <b>MAVEN_HOME</b> and <b>MAVEN_HOME/bin</b> to environmental variable.
   <a href="https://git-scm.com/">GIT</a> is required to clone the application.
   Use <b><i>git clone https://gitlab.com/suyojan_learn/microservice-example-zuul-proxy.git</i></b> to clone the application to local repository.
   This application is having four services
   <ol>
      <li>UI-Server: Provide UIs to interact with the system</li>
      <li>Product-Stock-Service: Provide stock information of products</li>
      <li>User-Management: Provide service for user management</li>
      <li>Order-Service: Provide list of orders</li>
   </ol>
 
 There are two more components are available
 <ol>
   <li>Discovery-Server: This is providing dynamic discovery using eureka</li>
   <li>zuul-proxy-server: This component provides proxy service</li>
 </ol>
 
 
 Mysql server version 5.7 is required for demonstration.<br>
 
 Create following schemas to store information
 
 <ul>
    <li>usermanagement: To maintain user</li>
    <li>stock: To maintain product information</li>
    <li>order: To maintain order</li>
 </ul>
   After schema creation start 
   <ul>
      <li>Discover-Server</li>
      <li>zuul-proxy-server</li>
   </ul>
   
   After starting the above services in order, start other services in any order. To start any of the service <b>mvn clean install</b> command can be used.<br>
   For interacting the application use http://localhost:8080/
</p>



