package com.learn.microservice.orderservice.repository;

import com.learn.microservice.orderservice.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order,Long>
{

    List<Order> findByUserid(String userid);
}
