package com.learn.microservices.uiserver.model;

import java.util.List;

public class OrderList {
    private List<Order> orderList;

    public List<Order> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<Order> orderList) {
        this.orderList = orderList;
    }
}
