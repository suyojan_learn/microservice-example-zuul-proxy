package com.learn.microservices.uiserver.model;

import java.util.List;

public class ProductVM
{
    private List<Product> productList;

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }
}
