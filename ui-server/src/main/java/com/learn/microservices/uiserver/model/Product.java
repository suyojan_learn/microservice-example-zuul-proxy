package com.learn.microservices.uiserver.model;

public class Product {
    private String productName;
    private String productCode;
    private float productPrice;

    private long noOfSample;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public float getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(float productPrice) {
        this.productPrice = productPrice;
    }

    public long getNoOfSample() {
        return noOfSample;
    }

    public void setNoOfSample(long noOfSample) {
        this.noOfSample = noOfSample;
    }
}
