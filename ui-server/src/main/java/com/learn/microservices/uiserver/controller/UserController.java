package com.learn.microservices.uiserver.controller;

import com.learn.microservices.uiserver.model.LoginVM;
import com.learn.microservices.uiserver.model.Message;
import com.learn.microservices.uiserver.model.ProductVM;
import com.learn.microservices.uiserver.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpSession;
import java.net.MalformedURLException;

@Controller
public class UserController
{
  @Autowired
  private RestTemplate restTemplate;
  @Autowired
  private DiscoveryClient discoveryClient;

  @GetMapping("/register")
  public String registrationPage(Model model)
  {
      model.addAttribute("user",new User());
      return "registration";
  }

  @PostMapping("/register")
  public String registerUser(@ModelAttribute("user")User user,Model model) throws MalformedURLException
  {
    String url = "http://zuul-proxy-server/usermanagement/register";
    ResponseEntity<Message> message=restTemplate.postForEntity(url,user, Message.class);
    if (message.getStatusCode() == HttpStatus.OK)
    {
       Message message1 = message.getBody();
       model.addAttribute("msg",message1.getMessage());
    }
    else
    {
        model.addAttribute("msg","there are some problem in registration.. kindly try after some times");
    }
    return "registration";
  }

  @GetMapping("/login")
  public String loginPage(Model model)
  {
      model.addAttribute("loginvm",new LoginVM());
      return "login";
  }

  @PostMapping("/login")
  public String login(@ModelAttribute("lginvm")LoginVM loginVM, Model model, HttpSession session) throws MalformedURLException
  {
      //ServiceInstance instance=discoveryClient.getInstances("zuul-proxy-server").get(0);
     // String baseURL=instance.getUri().toString();
      //String url = baseURL+"/usermanagement/login";
      String url = "http://zuul-proxy-server/usermanagement/login";
      ResponseEntity<Message> message = restTemplate.postForEntity(url,loginVM,Message.class);
      if (message.getStatusCode() == HttpStatus.OK)
      {
          Message message1 = message.getBody();
          if (message1.getErrorCode() == 1020)
          {
              String stock_url="http://zuul-proxy-server/product-stock/product-list";
              ResponseEntity<ProductVM> products = restTemplate.getForEntity(stock_url, ProductVM.class,new Object());
              if (products.getStatusCode() == HttpStatus.OK)
              {
                  session.setAttribute("userid",loginVM.getUserName());
                  model.addAttribute("productlist",products.getBody().getProductList());
              }
              return "inbox";
          }
          else
          {
              model.addAttribute("msg",message1.getMessage());
              model.addAttribute("loginvm",new LoginVM());
              return "login";
          }

      }
      else
      {
          model.addAttribute("msg","Something went wrong");
          return "home";
      }
  }

}
